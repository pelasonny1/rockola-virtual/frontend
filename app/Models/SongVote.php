<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SongVote extends Model
{
    protected $fillable = ['song_id', 'song_name', 'votes'];

    public static function getVotes($songId) {
        $song = self::where('song_id', $songId)->first();
        return $song ? $song->votes : 0;
    }
}
