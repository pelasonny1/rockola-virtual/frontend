<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;  // Importa el cliente Guzzle
use App\Models\SongVote;  // Asumiendo que estás usando este modelo para manejar votos

class SpotifyController extends Controller
{
    public function index()
    {
        $tracks = $this->getTracks();  // Usa getTracks para obtener las pistas

        foreach ($tracks as &$track) {
            $track['votes'] = SongVote::getVotes($track['id']);
        }

        return view('tracks.index', ['tracks' => $tracks]);  // Devuelve la vista con las pistas obtenidas
    }

    private function getTracks()
    {
        $client = new Client();  // Crea una instancia de Client
        $tracks = []; // Inicializa un array vacío para los tracks

        try {
            $response = $client->request('GET', 'http://localhost:8080/tracks');
            $tracks = json_decode($response->getBody()->getContents(), true);
            if (!is_array($tracks)) {  // Verifica si la respuesta es un array
                $tracks = [];
            }
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            \Log::error('Failed to fetch tracks: ' . $e->getMessage());  // Registra un error en caso de excepción
        }

        return $tracks;
    }
}
