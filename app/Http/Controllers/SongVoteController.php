<?php

namespace App\Http\Controllers;

use App\Models\SongVote;
use Illuminate\Http\Request;

class SongVoteController extends Controller
{
    public function vote(Request $request)
    {
        $songId = $request->input('song_id');
        $songName = $request->input('song_name');

        // Ensure songId is not null or empty
        if (empty($songId)) {
            return response()->json(['error' => 'Song ID is required'], 400);
        }

        $vote = SongVote::updateOrCreate(
            ['song_id' => $songId],
            ['song_name' => $songName, 'votes' => 0]
        );

        $vote->increment('votes');

        return response()->json(['success' => true, 'votes' => $vote->votes]);
    }

    public function topSongs()
    {
        $songs = SongVote::orderBy('votes', 'desc')->take(10)->get();
        return response()->json($songs);
    }
}
