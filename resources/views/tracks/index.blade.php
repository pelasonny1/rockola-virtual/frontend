@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @foreach ($tracks as $track)
            <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                    <div class="card-body">
                        <h5 class="card-title">{{ $track['name'] }}</h5>
                        <p class="card-text">{{ $track['artist'] }}</p>
                        <p class="card-text">Votes: {{ $track['votes'] }}</p>  <!-- Muestra los votos aquí -->
                        <button class="btn btn-primary vote-button" data-song-id="{{ $track['id'] }}" data-song-name="{{ $track['name'] }}">Vote</button>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>

@section('scripts')
<script>
$(document).ready(function() {
    $('.vote-button').click(function() {
    var button = $(this);
    var songId = button.data('song-id'); // This must correctly fetch the song ID
    var songName = button.data('song-name');

    if (!songId) {
        alert('Song ID is missing.');
        return;
    }

    $.ajax({
        url: '/vote',
        type: 'POST',
        data: {
            _token: $('meta[name="csrf-token"]').attr('content'),
            song_id: songId,
            song_name: songName
        },
        success: function(response) {
            alert('Thanks for voting!');
        },
        error: function(xhr) {
            alert('Error voting. ' + xhr.responseText);
        }
    });
});

});
</script>
@endsection
